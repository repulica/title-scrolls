package repulica.titlescrolls.mixin;

import dev.emi.trinkets.api.SlotReference;
import dev.emi.trinkets.api.TrinketsApi;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.util.Pair;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import repulica.titlescrolls.TitleScrollItem;

import net.minecraft.client.network.AbstractClientPlayerEntity;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.LivingEntityRenderer;
import net.minecraft.client.render.entity.PlayerEntityRenderer;
import net.minecraft.client.render.entity.model.PlayerEntityModel;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

import java.util.List;

@Environment(EnvType.CLIENT)
@Mixin(PlayerEntityRenderer.class)
public abstract class MixinPlayerEntityRenderer extends LivingEntityRenderer<AbstractClientPlayerEntity,
		PlayerEntityModel<AbstractClientPlayerEntity>> {

	public MixinPlayerEntityRenderer(EntityRendererFactory.Context context, PlayerEntityModel<AbstractClientPlayerEntity> entityModel, float f) {
		super(context, entityModel, f);
	}

	@Inject(method = "renderLabelIfPresent", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/network/AbstractClientPlayerEntity;getScoreboard()Lnet/minecraft/scoreboard/Scoreboard;"),
			cancellable = true)
	private void injectTitleRender(AbstractClientPlayerEntity player, Text text, MatrixStack matrices,
								   VertexConsumerProvider vertices, int i, CallbackInfo info) {
		List<Pair<SlotReference, ItemStack>> trinkets = TrinketsApi.getTrinketComponent(player).get().getEquipped(stack -> stack.getItem() instanceof TitleScrollItem);
		if (trinkets.size() > 0) {
			ItemStack stack = trinkets.get(0).getRight();
			Text title = ((TitleScrollItem) stack.getItem()).getTitle(stack);
			matrices.push();
			matrices.scale(0.75f, 0.75f, 0.75f);
			matrices.translate(0f, 0.6f, 0f);
			super.renderLabelIfPresent(player, title, matrices, vertices, i);
			matrices.pop();
			matrices.translate(0d, 0.1225d, 0d);
//			matrices.translate(0.0d, (9.0f * 1.15f * 0.025f), 0.0d);
			super.renderLabelIfPresent(player, text, matrices, vertices, i);
			matrices.pop();
			info.cancel();
		}
	}
}
