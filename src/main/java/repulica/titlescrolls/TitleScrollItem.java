package repulica.titlescrolls;

import java.util.List;

import dev.emi.trinkets.api.SlotReference;
import dev.emi.trinkets.api.Trinket;

import dev.emi.trinkets.api.TrinketItem;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

public class TitleScrollItem extends TrinketItem {

	public TitleScrollItem(Settings settings) {
		super(settings);
	}

	public Text getTitle(ItemStack stack) {
		return stack.getName();
	}

	@Override
	public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
		super.appendTooltip(stack, world, tooltip, context);
		if (this == TitleScrolls.TITLE_SCROLL && !stack.hasCustomName()) tooltip.add(new TranslatableText("message.titlescrolls.rename"));
	}

	public int getRibbonColor(ItemStack stack) {
		return MathHelper.hsvToRgb(Math.abs(getTitle(stack).asString().hashCode()) % 360 / 360f, 1f, 1f);
	}

	@Override
	public boolean canEquip(ItemStack stack, SlotReference slot, LivingEntity entity) {
		return stack.hasCustomName();
	}
}
