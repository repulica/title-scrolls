package repulica.titlescrolls;

import dev.emi.trinkets.api.SlotReference;
import dev.emi.trinkets.api.client.TrinketRenderer;
import dev.emi.trinkets.api.client.TrinketRendererRegistry;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.model.EntityModel;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import repulica.titlescrolls.api.TitleEffect;
import repulica.titlescrolls.api.TitleEffects;

import net.minecraft.particle.ParticleTypes;
import net.minecraft.util.Identifier;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.rendering.v1.ColorProviderRegistry;

public class TitleScrollsClient implements ClientModInitializer {

	@Override
	public void onInitializeClient() {
		ColorProviderRegistry.ITEM.register((stack, tintIndex) -> {
			if (tintIndex == 1) return ((TitleScrollItem) stack.getItem()).getRibbonColor(stack);
			return 0xFFFFFF;
		}, TitleScrolls.TITLE_SCROLL, TitleScrolls.UNCOMMON_TITLE_SCROLL, TitleScrolls.RARE_TITLE_SCROLL,
				TitleScrolls.EPIC_TITLE_SCROLL);
		ClientPlayNetworking.registerGlobalReceiver(TitleScrolls.TITLE_PACKET, (client, handler, buf, sender) -> TitleManager.INSTANCE.fromPacket(buf));
		TitleEffects.INSTANCE.registerEffect(new Identifier(TitleScrolls.MODID, "none"), TitleEffect.NONE);
		TitleEffects.INSTANCE.registerEffect(new Identifier(TitleScrolls.MODID, "ender"),
				((stack, slot, matrixStack, vertexConsumer, light, model, player, headYaw, headPitch) ->
						player.world.addParticle(ParticleTypes.PORTAL, player.getParticleX(0.5D),
								player.getRandomBodyY() - 0.25D, player.getParticleZ(0.5D),
								(player.getRandom().nextDouble() - 0.5D) * 2.0D,
								-player.getRandom().nextDouble(),
								(player.getRandom().nextDouble() - 0.5D) * 2.0D)
				)
		);
		TitleEffects.INSTANCE.registerEffect(new Identifier(TitleScrolls.MODID, "ashen"),
				((stack, slot, matrixStack, vertexConsumer, light, model, player, headYaw, headPitch) ->
						player.world.addParticle(ParticleTypes.ASH, player.getParticleX(0.5D),
								player.getRandomBodyY() - 0.25D, player.getParticleZ(0.5D),
								(player.getRandom().nextDouble() - 0.5D) * 2.0D,
								-player.getRandom().nextDouble(),
								(player.getRandom().nextDouble() - 0.5D) * 2.0D)
				)
		);
		//no-op because the player render mixin does it
		TitleEffects.INSTANCE.registerEffect(new Identifier(TitleScrolls.MODID, "phosphor"), TitleEffect.NONE);
		TrinketRenderer renderer = (ItemStack stack, SlotReference slotReference, EntityModel<? extends LivingEntity > contextModel,
				MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, LivingEntity entity,
				float limbAngle, float limbDistance, float tickDelta, float animationProgress, float headYaw,
				float headPitch) -> {
			if (stack.getOrCreateNbt().contains("Title", NbtType.STRING)) {
				Identifier id = new Identifier(stack.getOrCreateNbt().getString("Title"));
				Identifier effect = TitleManager.INSTANCE.getTitle(id).getEffect();
				TitleEffects.INSTANCE.getEffect(effect).render(stack, slotReference, matrices, vertexConsumers, light, contextModel,
						entity, headYaw, headPitch);
			}
		};
		TrinketRendererRegistry.registerRenderer(TitleScrolls.TITLE_SCROLL, renderer);
		TrinketRendererRegistry.registerRenderer(TitleScrolls.UNCOMMON_TITLE_SCROLL, renderer);
		TrinketRendererRegistry.registerRenderer(TitleScrolls.RARE_TITLE_SCROLL, renderer);
		TrinketRendererRegistry.registerRenderer(TitleScrolls.EPIC_TITLE_SCROLL, renderer);
	}
}
