package repulica.titlescrolls;

import dev.emi.trinkets.api.SlotReference;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.List;

public class SpecialTitleScrollItem extends TitleScrollItem {
	public SpecialTitleScrollItem(Settings settings) {
		super(settings);
	}

	@Override
	public Text getTitle(ItemStack stack) {
		if (stack.getOrCreateNbt().contains("Title", NbtType.STRING)) {
			Identifier id = new Identifier(stack.getOrCreateNbt().getString("Title"));
			return TitleManager.INSTANCE.getTitle(id).getText();
		}
		return super.getTitle(stack);
	}

	@Override
	public int getRibbonColor(ItemStack stack) {
		if (stack.getOrCreateNbt().contains("Title", NbtType.STRING)) {
			Identifier id = new Identifier(stack.getOrCreateNbt().getString("Title"));
			return TitleManager.INSTANCE.getTitle(id).getColor();
		}
		return super.getRibbonColor(stack);
	}

	@Override
	public boolean canEquip(ItemStack stack, SlotReference slot, LivingEntity entity) {
		return true;
	}

	@Override
	public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
		super.appendTooltip(stack, world, tooltip, context);
		tooltip.add(new TranslatableText("message.titlescrolls.title", getTitle(stack)));
		if (stack.getOrCreateNbt().contains("Title", NbtType.STRING)) {
			Identifier id = new Identifier(stack.getOrCreateNbt().getString("Title"));
			tooltip.addAll(TitleManager.INSTANCE.getTitle(id).getLore());
		}
	}

	@Override
	public boolean hasGlint(ItemStack stack) {
		return true;
	}

	@Nullable
	public Identifier getTitleId(ItemStack stack) {
		if (stack.getOrCreateNbt().contains("Title", NbtType.STRING)) {
			return new Identifier(stack.getOrCreateNbt().getString("Title"));
		}
		return null;
	}
}
